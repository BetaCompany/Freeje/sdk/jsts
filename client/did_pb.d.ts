// package: com.freeje.api.client
// file: client/did.proto

import * as jspb from "google-protobuf";
import * as common_pb from "../common_pb";

export class Language extends jspb.Message {
  getCode(): string;
  setCode(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Language.AsObject;
  static toObject(includeInstance: boolean, msg: Language): Language.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Language, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Language;
  static deserializeBinaryFromReader(message: Language, reader: jspb.BinaryReader): Language;
}

export namespace Language {
  export type AsObject = {
    code: string,
  }
}

export class DidCountry extends jspb.Message {
  getCountry(): string;
  setCountry(value: string): void;

  getPrefix(): string;
  setPrefix(value: string): void;

  getLabel(): string;
  setLabel(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DidCountry.AsObject;
  static toObject(includeInstance: boolean, msg: DidCountry): DidCountry.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DidCountry, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DidCountry;
  static deserializeBinaryFromReader(message: DidCountry, reader: jspb.BinaryReader): DidCountry;
}

export namespace DidCountry {
  export type AsObject = {
    country: string,
    prefix: string,
    label: string,
  }
}

export class DidLocality extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getPrefix(): string;
  setPrefix(value: string): void;

  getCountry(): string;
  setCountry(value: string): void;

  getLabel(): string;
  setLabel(value: string): void;

  clearFeaturesList(): void;
  getFeaturesList(): Array<common_pb.NumberFeatureMap[keyof common_pb.NumberFeatureMap]>;
  setFeaturesList(value: Array<common_pb.NumberFeatureMap[keyof common_pb.NumberFeatureMap]>): void;
  addFeatures(value: common_pb.NumberFeatureMap[keyof common_pb.NumberFeatureMap], index?: number): common_pb.NumberFeatureMap[keyof common_pb.NumberFeatureMap];

  clearTypesList(): void;
  getTypesList(): Array<common_pb.PhoneTypeMap[keyof common_pb.PhoneTypeMap]>;
  setTypesList(value: Array<common_pb.PhoneTypeMap[keyof common_pb.PhoneTypeMap]>): void;
  addTypes(value: common_pb.PhoneTypeMap[keyof common_pb.PhoneTypeMap], index?: number): common_pb.PhoneTypeMap[keyof common_pb.PhoneTypeMap];

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DidLocality.AsObject;
  static toObject(includeInstance: boolean, msg: DidLocality): DidLocality.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DidLocality, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DidLocality;
  static deserializeBinaryFromReader(message: DidLocality, reader: jspb.BinaryReader): DidLocality;
}

export namespace DidLocality {
  export type AsObject = {
    id: string,
    prefix: string,
    country: string,
    label: string,
    featuresList: Array<common_pb.NumberFeatureMap[keyof common_pb.NumberFeatureMap]>,
    typesList: Array<common_pb.PhoneTypeMap[keyof common_pb.PhoneTypeMap]>,
  }
}

export class DidRequest extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DidRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DidRequest): DidRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DidRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DidRequest;
  static deserializeBinaryFromReader(message: DidRequest, reader: jspb.BinaryReader): DidRequest;
}

export namespace DidRequest {
  export type AsObject = {
    id: string,
  }
}

export class OfferBasket extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  hasOffer(): boolean;
  clearOffer(): void;
  getOffer(): DidOffer | undefined;
  setOffer(value?: DidOffer): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): OfferBasket.AsObject;
  static toObject(includeInstance: boolean, msg: OfferBasket): OfferBasket.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: OfferBasket, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): OfferBasket;
  static deserializeBinaryFromReader(message: OfferBasket, reader: jspb.BinaryReader): OfferBasket;
}

export namespace OfferBasket {
  export type AsObject = {
    id: string,
    offer?: DidOffer.AsObject,
  }
}

export class DidOffer extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getNumber(): string;
  setNumber(value: string): void;

  getType(): common_pb.PhoneTypeMap[keyof common_pb.PhoneTypeMap];
  setType(value: common_pb.PhoneTypeMap[keyof common_pb.PhoneTypeMap]): void;

  hasPrice(): boolean;
  clearPrice(): void;
  getPrice(): common_pb.DidPrice | undefined;
  setPrice(value?: common_pb.DidPrice): void;

  clearRequirementsList(): void;
  getRequirementsList(): Array<common_pb.DocumentTypeMap[keyof common_pb.DocumentTypeMap]>;
  setRequirementsList(value: Array<common_pb.DocumentTypeMap[keyof common_pb.DocumentTypeMap]>): void;
  addRequirements(value: common_pb.DocumentTypeMap[keyof common_pb.DocumentTypeMap], index?: number): common_pb.DocumentTypeMap[keyof common_pb.DocumentTypeMap];

  clearFeaturesList(): void;
  getFeaturesList(): Array<common_pb.NumberFeatureMap[keyof common_pb.NumberFeatureMap]>;
  setFeaturesList(value: Array<common_pb.NumberFeatureMap[keyof common_pb.NumberFeatureMap]>): void;
  addFeatures(value: common_pb.NumberFeatureMap[keyof common_pb.NumberFeatureMap], index?: number): common_pb.NumberFeatureMap[keyof common_pb.NumberFeatureMap];

  clearDocumentIdsList(): void;
  getDocumentIdsList(): Array<string>;
  setDocumentIdsList(value: Array<string>): void;
  addDocumentIds(value: string, index?: number): string;

  getNotes(): string;
  setNotes(value: string): void;

  getCountry(): string;
  setCountry(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DidOffer.AsObject;
  static toObject(includeInstance: boolean, msg: DidOffer): DidOffer.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DidOffer, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DidOffer;
  static deserializeBinaryFromReader(message: DidOffer, reader: jspb.BinaryReader): DidOffer;
}

export namespace DidOffer {
  export type AsObject = {
    id: string,
    number: string,
    type: common_pb.PhoneTypeMap[keyof common_pb.PhoneTypeMap],
    price?: common_pb.DidPrice.AsObject,
    requirementsList: Array<common_pb.DocumentTypeMap[keyof common_pb.DocumentTypeMap]>,
    featuresList: Array<common_pb.NumberFeatureMap[keyof common_pb.NumberFeatureMap]>,
    documentIdsList: Array<string>,
    notes: string,
    country: string,
  }
}

export class LocalityConstraints extends jspb.Message {
  getCountry(): string;
  setCountry(value: string): void;

  getPrefix(): string;
  setPrefix(value: string): void;

  clearFeaturesList(): void;
  getFeaturesList(): Array<common_pb.NumberFeatureMap[keyof common_pb.NumberFeatureMap]>;
  setFeaturesList(value: Array<common_pb.NumberFeatureMap[keyof common_pb.NumberFeatureMap]>): void;
  addFeatures(value: common_pb.NumberFeatureMap[keyof common_pb.NumberFeatureMap], index?: number): common_pb.NumberFeatureMap[keyof common_pb.NumberFeatureMap];

  clearTypesList(): void;
  getTypesList(): Array<common_pb.PhoneTypeMap[keyof common_pb.PhoneTypeMap]>;
  setTypesList(value: Array<common_pb.PhoneTypeMap[keyof common_pb.PhoneTypeMap]>): void;
  addTypes(value: common_pb.PhoneTypeMap[keyof common_pb.PhoneTypeMap], index?: number): common_pb.PhoneTypeMap[keyof common_pb.PhoneTypeMap];

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LocalityConstraints.AsObject;
  static toObject(includeInstance: boolean, msg: LocalityConstraints): LocalityConstraints.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: LocalityConstraints, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LocalityConstraints;
  static deserializeBinaryFromReader(message: LocalityConstraints, reader: jspb.BinaryReader): LocalityConstraints;
}

export namespace LocalityConstraints {
  export type AsObject = {
    country: string,
    prefix: string,
    featuresList: Array<common_pb.NumberFeatureMap[keyof common_pb.NumberFeatureMap]>,
    typesList: Array<common_pb.PhoneTypeMap[keyof common_pb.PhoneTypeMap]>,
  }
}

export class DidNumber extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getNumber(): string;
  setNumber(value: string): void;

  getLabel(): string;
  setLabel(value: string): void;

  getCountry(): string;
  setCountry(value: string): void;

  getPayedTill(): number;
  setPayedTill(value: number): void;

  getCreatedms(): number;
  setCreatedms(value: number): void;

  getLocality(): string;
  setLocality(value: string): void;

  getType(): common_pb.PhoneTypeMap[keyof common_pb.PhoneTypeMap];
  setType(value: common_pb.PhoneTypeMap[keyof common_pb.PhoneTypeMap]): void;

  hasPrice(): boolean;
  clearPrice(): void;
  getPrice(): common_pb.DidPrice | undefined;
  setPrice(value?: common_pb.DidPrice): void;

  getNotes(): string;
  setNotes(value: string): void;

  clearFeaturesList(): void;
  getFeaturesList(): Array<common_pb.NumberFeatureMap[keyof common_pb.NumberFeatureMap]>;
  setFeaturesList(value: Array<common_pb.NumberFeatureMap[keyof common_pb.NumberFeatureMap]>): void;
  addFeatures(value: common_pb.NumberFeatureMap[keyof common_pb.NumberFeatureMap], index?: number): common_pb.NumberFeatureMap[keyof common_pb.NumberFeatureMap];

  getStatus(): common_pb.DidStatusMap[keyof common_pb.DidStatusMap];
  setStatus(value: common_pb.DidStatusMap[keyof common_pb.DidStatusMap]): void;

  getIncomplete(): boolean;
  setIncomplete(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DidNumber.AsObject;
  static toObject(includeInstance: boolean, msg: DidNumber): DidNumber.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DidNumber, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DidNumber;
  static deserializeBinaryFromReader(message: DidNumber, reader: jspb.BinaryReader): DidNumber;
}

export namespace DidNumber {
  export type AsObject = {
    id: string,
    number: string,
    label: string,
    country: string,
    payedTill: number,
    createdms: number,
    locality: string,
    type: common_pb.PhoneTypeMap[keyof common_pb.PhoneTypeMap],
    price?: common_pb.DidPrice.AsObject,
    notes: string,
    featuresList: Array<common_pb.NumberFeatureMap[keyof common_pb.NumberFeatureMap]>,
    status: common_pb.DidStatusMap[keyof common_pb.DidStatusMap],
    incomplete: boolean,
  }
}

