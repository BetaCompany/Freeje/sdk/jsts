// package: com.freeje.api.client
// file: client/payment.proto

import * as jspb from "google-protobuf";

export class Method extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getDescription(): string;
  setDescription(value: string): void;

  getRequest(): string;
  setRequest(value: string): void;

  getAmount(): number;
  setAmount(value: number): void;

  getFinalAmount(): number;
  setFinalAmount(value: number): void;

  getCurrency(): string;
  setCurrency(value: string): void;

  getType(): string;
  setType(value: string): void;

  getRate(): number;
  setRate(value: number): void;

  getGeo(): string;
  setGeo(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Method.AsObject;
  static toObject(includeInstance: boolean, msg: Method): Method.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Method, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Method;
  static deserializeBinaryFromReader(message: Method, reader: jspb.BinaryReader): Method;
}

export namespace Method {
  export type AsObject = {
    id: string,
    description: string,
    request: string,
    amount: number,
    finalAmount: number,
    currency: string,
    type: string,
    rate: number,
    geo: string,
  }
}

export class Invoice extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getPayId(): string;
  setPayId(value: string): void;

  getRequest(): string;
  setRequest(value: string): void;

  getProduct(): string;
  setProduct(value: string): void;

  getAmount(): number;
  setAmount(value: number): void;

  getFinalAmount(): number;
  setFinalAmount(value: number): void;

  getCurrency(): string;
  setCurrency(value: string): void;

  getActionUrl(): string;
  setActionUrl(value: string): void;

  getActionMethod(): string;
  setActionMethod(value: string): void;

  getParametersMap(): jspb.Map<string, string>;
  clearParametersMap(): void;
  getRate(): number;
  setRate(value: number): void;

  getSuccessUrl(): string;
  setSuccessUrl(value: string): void;

  getFailUrl(): string;
  setFailUrl(value: string): void;

  getName(): string;
  setName(value: string): void;

  getUrl(): string;
  setUrl(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Invoice.AsObject;
  static toObject(includeInstance: boolean, msg: Invoice): Invoice.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Invoice, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Invoice;
  static deserializeBinaryFromReader(message: Invoice, reader: jspb.BinaryReader): Invoice;
}

export namespace Invoice {
  export type AsObject = {
    id: string,
    payId: string,
    request: string,
    product: string,
    amount: number,
    finalAmount: number,
    currency: string,
    actionUrl: string,
    actionMethod: string,
    parametersMap: Array<[string, string]>,
    rate: number,
    successUrl: string,
    failUrl: string,
    name: string,
    url: string,
  }
}

export class StripeData extends jspb.Message {
  getCurrency(): string;
  setCurrency(value: string): void;

  getAmount(): number;
  setAmount(value: number): void;

  getToken(): string;
  setToken(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StripeData.AsObject;
  static toObject(includeInstance: boolean, msg: StripeData): StripeData.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: StripeData, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StripeData;
  static deserializeBinaryFromReader(message: StripeData, reader: jspb.BinaryReader): StripeData;
}

export namespace StripeData {
  export type AsObject = {
    currency: string,
    amount: number,
    token: string,
  }
}

export class StripeResult extends jspb.Message {
  getSuccess(): boolean;
  setSuccess(value: boolean): void;

  getCode(): string;
  setCode(value: string): void;

  getMessage(): string;
  setMessage(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StripeResult.AsObject;
  static toObject(includeInstance: boolean, msg: StripeResult): StripeResult.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: StripeResult, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StripeResult;
  static deserializeBinaryFromReader(message: StripeResult, reader: jspb.BinaryReader): StripeResult;
}

export namespace StripeResult {
  export type AsObject = {
    success: boolean,
    code: string,
    message: string,
  }
}

export class ApplePayData extends jspb.Message {
  getJsonData(): string;
  setJsonData(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ApplePayData.AsObject;
  static toObject(includeInstance: boolean, msg: ApplePayData): ApplePayData.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ApplePayData, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ApplePayData;
  static deserializeBinaryFromReader(message: ApplePayData, reader: jspb.BinaryReader): ApplePayData;
}

export namespace ApplePayData {
  export type AsObject = {
    jsonData: string,
  }
}

export class ApplePayResult extends jspb.Message {
  getCode(): number;
  setCode(value: number): void;

  getMessage(): string;
  setMessage(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ApplePayResult.AsObject;
  static toObject(includeInstance: boolean, msg: ApplePayResult): ApplePayResult.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ApplePayResult, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ApplePayResult;
  static deserializeBinaryFromReader(message: ApplePayResult, reader: jspb.BinaryReader): ApplePayResult;
}

export namespace ApplePayResult {
  export type AsObject = {
    code: number,
    message: string,
  }
}

