// package: com.freeje.api.client
// file: client/profile.proto

import * as client_profile_pb from "../client/profile_pb";
import * as common_pb from "../common_pb";
import {grpc} from "@improbable-eng/grpc-web";

type ProfileListPhones = {
  readonly methodName: string;
  readonly service: typeof Profile;
  readonly requestStream: false;
  readonly responseStream: true;
  readonly requestType: typeof common_pb.RequestMode;
  readonly responseType: typeof common_pb.PhoneNumber;
};

type ProfileLinkPhone = {
  readonly methodName: string;
  readonly service: typeof Profile;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof client_profile_pb.PhoneToken;
  readonly responseType: typeof common_pb.Empty;
};

type ProfileUnlinkPhone = {
  readonly methodName: string;
  readonly service: typeof Profile;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof common_pb.PhoneNumber;
  readonly responseType: typeof common_pb.Empty;
};

type ProfileLinkPushId = {
  readonly methodName: string;
  readonly service: typeof Profile;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof common_pb.PushId;
  readonly responseType: typeof common_pb.Empty;
};

type ProfileUnLinkPushId = {
  readonly methodName: string;
  readonly service: typeof Profile;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof common_pb.PushId;
  readonly responseType: typeof common_pb.Empty;
};

type ProfileListPushId = {
  readonly methodName: string;
  readonly service: typeof Profile;
  readonly requestStream: false;
  readonly responseStream: true;
  readonly requestType: typeof common_pb.RequestMode;
  readonly responseType: typeof common_pb.PushId;
};

type ProfileUpdateAccessCredentials = {
  readonly methodName: string;
  readonly service: typeof Profile;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof client_profile_pb.AccessCredentials;
  readonly responseType: typeof client_profile_pb.AccessCredentials;
};

type ProfileListAccessCredentials = {
  readonly methodName: string;
  readonly service: typeof Profile;
  readonly requestStream: false;
  readonly responseStream: true;
  readonly requestType: typeof common_pb.RequestMode;
  readonly responseType: typeof client_profile_pb.AccessCredentials;
};

type ProfileRevokeAccessCredentials = {
  readonly methodName: string;
  readonly service: typeof Profile;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof client_profile_pb.AccessCredentials;
  readonly responseType: typeof client_profile_pb.AccessCredentials;
};

type ProfileSelf = {
  readonly methodName: string;
  readonly service: typeof Profile;
  readonly requestStream: false;
  readonly responseStream: true;
  readonly requestType: typeof common_pb.RequestMode;
  readonly responseType: typeof client_profile_pb.AccountProfile;
};

type ProfileBuildFinanceReport = {
  readonly methodName: string;
  readonly service: typeof Profile;
  readonly requestStream: false;
  readonly responseStream: true;
  readonly requestType: typeof common_pb.ReportRequestOptions;
  readonly responseType: typeof common_pb.FinanceOperation;
};

type ProfileSetName = {
  readonly methodName: string;
  readonly service: typeof Profile;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof client_profile_pb.AccountProfile;
  readonly responseType: typeof client_profile_pb.AccountProfile;
};

type ProfileGetProfileDocuments = {
  readonly methodName: string;
  readonly service: typeof Profile;
  readonly requestStream: false;
  readonly responseStream: true;
  readonly requestType: typeof common_pb.RequestMode;
  readonly responseType: typeof client_profile_pb.ProfileDocument;
};

type ProfileGetProfileDocument = {
  readonly methodName: string;
  readonly service: typeof Profile;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof client_profile_pb.ProfileDocument;
  readonly responseType: typeof client_profile_pb.ProfileDocument;
};

type ProfileUpdateProfileDocument = {
  readonly methodName: string;
  readonly service: typeof Profile;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof client_profile_pb.ProfileDocument;
  readonly responseType: typeof client_profile_pb.ProfileDocument;
};

type ProfileDeleteProfileDocument = {
  readonly methodName: string;
  readonly service: typeof Profile;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof client_profile_pb.ProfileDocument;
  readonly responseType: typeof client_profile_pb.ProfileDocument;
};

type ProfileAcceptAgreement = {
  readonly methodName: string;
  readonly service: typeof Profile;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof common_pb.AccountAgreement;
  readonly responseType: typeof common_pb.AccountAgreement;
};

type ProfileGetLimits = {
  readonly methodName: string;
  readonly service: typeof Profile;
  readonly requestStream: false;
  readonly responseStream: true;
  readonly requestType: typeof common_pb.RequestMode;
  readonly responseType: typeof client_profile_pb.Limit;
};

export class Profile {
  static readonly serviceName: string;
  static readonly ListPhones: ProfileListPhones;
  static readonly LinkPhone: ProfileLinkPhone;
  static readonly UnlinkPhone: ProfileUnlinkPhone;
  static readonly LinkPushId: ProfileLinkPushId;
  static readonly UnLinkPushId: ProfileUnLinkPushId;
  static readonly ListPushId: ProfileListPushId;
  static readonly UpdateAccessCredentials: ProfileUpdateAccessCredentials;
  static readonly ListAccessCredentials: ProfileListAccessCredentials;
  static readonly RevokeAccessCredentials: ProfileRevokeAccessCredentials;
  static readonly Self: ProfileSelf;
  static readonly BuildFinanceReport: ProfileBuildFinanceReport;
  static readonly SetName: ProfileSetName;
  static readonly GetProfileDocuments: ProfileGetProfileDocuments;
  static readonly GetProfileDocument: ProfileGetProfileDocument;
  static readonly UpdateProfileDocument: ProfileUpdateProfileDocument;
  static readonly DeleteProfileDocument: ProfileDeleteProfileDocument;
  static readonly AcceptAgreement: ProfileAcceptAgreement;
  static readonly GetLimits: ProfileGetLimits;
}

export type ServiceError = { message: string, code: number; metadata: grpc.Metadata }
export type Status = { details: string, code: number; metadata: grpc.Metadata }

interface UnaryResponse {
  cancel(): void;
}
interface ResponseStream<T> {
  cancel(): void;
  on(type: 'data', handler: (message: T) => void): ResponseStream<T>;
  on(type: 'end', handler: (status?: Status) => void): ResponseStream<T>;
  on(type: 'status', handler: (status: Status) => void): ResponseStream<T>;
}
interface RequestStream<T> {
  write(message: T): RequestStream<T>;
  end(): void;
  cancel(): void;
  on(type: 'end', handler: (status?: Status) => void): RequestStream<T>;
  on(type: 'status', handler: (status: Status) => void): RequestStream<T>;
}
interface BidirectionalStream<ReqT, ResT> {
  write(message: ReqT): BidirectionalStream<ReqT, ResT>;
  end(): void;
  cancel(): void;
  on(type: 'data', handler: (message: ResT) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'end', handler: (status?: Status) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'status', handler: (status: Status) => void): BidirectionalStream<ReqT, ResT>;
}

export class ProfileClient {
  readonly serviceHost: string;

  constructor(serviceHost: string, options?: grpc.RpcOptions);
  listPhones(requestMessage: common_pb.RequestMode, metadata?: grpc.Metadata): ResponseStream<common_pb.PhoneNumber>;
  linkPhone(
    requestMessage: client_profile_pb.PhoneToken,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: common_pb.Empty|null) => void
  ): UnaryResponse;
  linkPhone(
    requestMessage: client_profile_pb.PhoneToken,
    callback: (error: ServiceError|null, responseMessage: common_pb.Empty|null) => void
  ): UnaryResponse;
  unlinkPhone(
    requestMessage: common_pb.PhoneNumber,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: common_pb.Empty|null) => void
  ): UnaryResponse;
  unlinkPhone(
    requestMessage: common_pb.PhoneNumber,
    callback: (error: ServiceError|null, responseMessage: common_pb.Empty|null) => void
  ): UnaryResponse;
  linkPushId(
    requestMessage: common_pb.PushId,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: common_pb.Empty|null) => void
  ): UnaryResponse;
  linkPushId(
    requestMessage: common_pb.PushId,
    callback: (error: ServiceError|null, responseMessage: common_pb.Empty|null) => void
  ): UnaryResponse;
  unLinkPushId(
    requestMessage: common_pb.PushId,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: common_pb.Empty|null) => void
  ): UnaryResponse;
  unLinkPushId(
    requestMessage: common_pb.PushId,
    callback: (error: ServiceError|null, responseMessage: common_pb.Empty|null) => void
  ): UnaryResponse;
  listPushId(requestMessage: common_pb.RequestMode, metadata?: grpc.Metadata): ResponseStream<common_pb.PushId>;
  updateAccessCredentials(
    requestMessage: client_profile_pb.AccessCredentials,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: client_profile_pb.AccessCredentials|null) => void
  ): UnaryResponse;
  updateAccessCredentials(
    requestMessage: client_profile_pb.AccessCredentials,
    callback: (error: ServiceError|null, responseMessage: client_profile_pb.AccessCredentials|null) => void
  ): UnaryResponse;
  listAccessCredentials(requestMessage: common_pb.RequestMode, metadata?: grpc.Metadata): ResponseStream<client_profile_pb.AccessCredentials>;
  revokeAccessCredentials(
    requestMessage: client_profile_pb.AccessCredentials,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: client_profile_pb.AccessCredentials|null) => void
  ): UnaryResponse;
  revokeAccessCredentials(
    requestMessage: client_profile_pb.AccessCredentials,
    callback: (error: ServiceError|null, responseMessage: client_profile_pb.AccessCredentials|null) => void
  ): UnaryResponse;
  self(requestMessage: common_pb.RequestMode, metadata?: grpc.Metadata): ResponseStream<client_profile_pb.AccountProfile>;
  buildFinanceReport(requestMessage: common_pb.ReportRequestOptions, metadata?: grpc.Metadata): ResponseStream<common_pb.FinanceOperation>;
  setName(
    requestMessage: client_profile_pb.AccountProfile,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: client_profile_pb.AccountProfile|null) => void
  ): UnaryResponse;
  setName(
    requestMessage: client_profile_pb.AccountProfile,
    callback: (error: ServiceError|null, responseMessage: client_profile_pb.AccountProfile|null) => void
  ): UnaryResponse;
  getProfileDocuments(requestMessage: common_pb.RequestMode, metadata?: grpc.Metadata): ResponseStream<client_profile_pb.ProfileDocument>;
  getProfileDocument(
    requestMessage: client_profile_pb.ProfileDocument,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: client_profile_pb.ProfileDocument|null) => void
  ): UnaryResponse;
  getProfileDocument(
    requestMessage: client_profile_pb.ProfileDocument,
    callback: (error: ServiceError|null, responseMessage: client_profile_pb.ProfileDocument|null) => void
  ): UnaryResponse;
  updateProfileDocument(
    requestMessage: client_profile_pb.ProfileDocument,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: client_profile_pb.ProfileDocument|null) => void
  ): UnaryResponse;
  updateProfileDocument(
    requestMessage: client_profile_pb.ProfileDocument,
    callback: (error: ServiceError|null, responseMessage: client_profile_pb.ProfileDocument|null) => void
  ): UnaryResponse;
  deleteProfileDocument(
    requestMessage: client_profile_pb.ProfileDocument,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: client_profile_pb.ProfileDocument|null) => void
  ): UnaryResponse;
  deleteProfileDocument(
    requestMessage: client_profile_pb.ProfileDocument,
    callback: (error: ServiceError|null, responseMessage: client_profile_pb.ProfileDocument|null) => void
  ): UnaryResponse;
  acceptAgreement(
    requestMessage: common_pb.AccountAgreement,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: common_pb.AccountAgreement|null) => void
  ): UnaryResponse;
  acceptAgreement(
    requestMessage: common_pb.AccountAgreement,
    callback: (error: ServiceError|null, responseMessage: common_pb.AccountAgreement|null) => void
  ): UnaryResponse;
  getLimits(requestMessage: common_pb.RequestMode, metadata?: grpc.Metadata): ResponseStream<client_profile_pb.Limit>;
}

