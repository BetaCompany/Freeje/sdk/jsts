// package: com.freeje.api.stor.kv
// file: stor/kv.proto

import * as stor_kv_pb from "../stor/kv_pb";
import {grpc} from "@improbable-eng/grpc-web";

type KVGet = {
  readonly methodName: string;
  readonly service: typeof KV;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof stor_kv_pb.Pair;
  readonly responseType: typeof stor_kv_pb.Pair;
};

type KVPut = {
  readonly methodName: string;
  readonly service: typeof KV;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof stor_kv_pb.Pair;
  readonly responseType: typeof stor_kv_pb.Pair;
};

type KVSetAdd = {
  readonly methodName: string;
  readonly service: typeof KV;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof stor_kv_pb.Pair;
  readonly responseType: typeof stor_kv_pb.Pair;
};

type KVSetDel = {
  readonly methodName: string;
  readonly service: typeof KV;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof stor_kv_pb.Pair;
  readonly responseType: typeof stor_kv_pb.Pair;
};

type KVSetClear = {
  readonly methodName: string;
  readonly service: typeof KV;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof stor_kv_pb.Pair;
  readonly responseType: typeof stor_kv_pb.Pair;
};

type KVSetGet = {
  readonly methodName: string;
  readonly service: typeof KV;
  readonly requestStream: false;
  readonly responseStream: true;
  readonly requestType: typeof stor_kv_pb.Pair;
  readonly responseType: typeof stor_kv_pb.Pair;
};

export class KV {
  static readonly serviceName: string;
  static readonly Get: KVGet;
  static readonly Put: KVPut;
  static readonly SetAdd: KVSetAdd;
  static readonly SetDel: KVSetDel;
  static readonly SetClear: KVSetClear;
  static readonly SetGet: KVSetGet;
}

export type ServiceError = { message: string, code: number; metadata: grpc.Metadata }
export type Status = { details: string, code: number; metadata: grpc.Metadata }

interface UnaryResponse {
  cancel(): void;
}
interface ResponseStream<T> {
  cancel(): void;
  on(type: 'data', handler: (message: T) => void): ResponseStream<T>;
  on(type: 'end', handler: (status?: Status) => void): ResponseStream<T>;
  on(type: 'status', handler: (status: Status) => void): ResponseStream<T>;
}
interface RequestStream<T> {
  write(message: T): RequestStream<T>;
  end(): void;
  cancel(): void;
  on(type: 'end', handler: (status?: Status) => void): RequestStream<T>;
  on(type: 'status', handler: (status: Status) => void): RequestStream<T>;
}
interface BidirectionalStream<ReqT, ResT> {
  write(message: ReqT): BidirectionalStream<ReqT, ResT>;
  end(): void;
  cancel(): void;
  on(type: 'data', handler: (message: ResT) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'end', handler: (status?: Status) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'status', handler: (status: Status) => void): BidirectionalStream<ReqT, ResT>;
}

export class KVClient {
  readonly serviceHost: string;

  constructor(serviceHost: string, options?: grpc.RpcOptions);
  get(
    requestMessage: stor_kv_pb.Pair,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: stor_kv_pb.Pair|null) => void
  ): UnaryResponse;
  get(
    requestMessage: stor_kv_pb.Pair,
    callback: (error: ServiceError|null, responseMessage: stor_kv_pb.Pair|null) => void
  ): UnaryResponse;
  put(
    requestMessage: stor_kv_pb.Pair,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: stor_kv_pb.Pair|null) => void
  ): UnaryResponse;
  put(
    requestMessage: stor_kv_pb.Pair,
    callback: (error: ServiceError|null, responseMessage: stor_kv_pb.Pair|null) => void
  ): UnaryResponse;
  setAdd(
    requestMessage: stor_kv_pb.Pair,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: stor_kv_pb.Pair|null) => void
  ): UnaryResponse;
  setAdd(
    requestMessage: stor_kv_pb.Pair,
    callback: (error: ServiceError|null, responseMessage: stor_kv_pb.Pair|null) => void
  ): UnaryResponse;
  setDel(
    requestMessage: stor_kv_pb.Pair,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: stor_kv_pb.Pair|null) => void
  ): UnaryResponse;
  setDel(
    requestMessage: stor_kv_pb.Pair,
    callback: (error: ServiceError|null, responseMessage: stor_kv_pb.Pair|null) => void
  ): UnaryResponse;
  setClear(
    requestMessage: stor_kv_pb.Pair,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: stor_kv_pb.Pair|null) => void
  ): UnaryResponse;
  setClear(
    requestMessage: stor_kv_pb.Pair,
    callback: (error: ServiceError|null, responseMessage: stor_kv_pb.Pair|null) => void
  ): UnaryResponse;
  setGet(requestMessage: stor_kv_pb.Pair, metadata?: grpc.Metadata): ResponseStream<stor_kv_pb.Pair>;
}

