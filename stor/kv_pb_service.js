// package: com.freeje.api.stor.kv
// file: stor/kv.proto

var stor_kv_pb = require("../stor/kv_pb");
var grpc = require("@improbable-eng/grpc-web").grpc;

var KV = (function () {
  function KV() {}
  KV.serviceName = "com.freeje.api.stor.kv.KV";
  return KV;
}());

KV.Get = {
  methodName: "Get",
  service: KV,
  requestStream: false,
  responseStream: false,
  requestType: stor_kv_pb.Pair,
  responseType: stor_kv_pb.Pair
};

KV.Put = {
  methodName: "Put",
  service: KV,
  requestStream: false,
  responseStream: false,
  requestType: stor_kv_pb.Pair,
  responseType: stor_kv_pb.Pair
};

KV.SetAdd = {
  methodName: "SetAdd",
  service: KV,
  requestStream: false,
  responseStream: false,
  requestType: stor_kv_pb.Pair,
  responseType: stor_kv_pb.Pair
};

KV.SetDel = {
  methodName: "SetDel",
  service: KV,
  requestStream: false,
  responseStream: false,
  requestType: stor_kv_pb.Pair,
  responseType: stor_kv_pb.Pair
};

KV.SetClear = {
  methodName: "SetClear",
  service: KV,
  requestStream: false,
  responseStream: false,
  requestType: stor_kv_pb.Pair,
  responseType: stor_kv_pb.Pair
};

KV.SetGet = {
  methodName: "SetGet",
  service: KV,
  requestStream: false,
  responseStream: true,
  requestType: stor_kv_pb.Pair,
  responseType: stor_kv_pb.Pair
};

exports.KV = KV;

function KVClient(serviceHost, options) {
  this.serviceHost = serviceHost;
  this.options = options || {};
}

KVClient.prototype.get = function get(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(KV.Get, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

KVClient.prototype.put = function put(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(KV.Put, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

KVClient.prototype.setAdd = function setAdd(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(KV.SetAdd, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

KVClient.prototype.setDel = function setDel(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(KV.SetDel, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

KVClient.prototype.setClear = function setClear(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(KV.SetClear, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

KVClient.prototype.setGet = function setGet(requestMessage, metadata) {
  var listeners = {
    data: [],
    end: [],
    status: []
  };
  var client = grpc.invoke(KV.SetGet, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onMessage: function (responseMessage) {
      listeners.data.forEach(function (handler) {
        handler(responseMessage);
      });
    },
    onEnd: function (status, statusMessage, trailers) {
      listeners.status.forEach(function (handler) {
        handler({ code: status, details: statusMessage, metadata: trailers });
      });
      listeners.end.forEach(function (handler) {
        handler({ code: status, details: statusMessage, metadata: trailers });
      });
      listeners = null;
    }
  });
  return {
    on: function (type, handler) {
      listeners[type].push(handler);
      return this;
    },
    cancel: function () {
      listeners = null;
      client.close();
    }
  };
};

exports.KVClient = KVClient;

